module PricingPageHelpers
  def plan_modal_options(plan)
    free = plan.id == 'free'

    {
      id: "#{plan.id}-modal",
      hosted_text: free ? 'Sign-up for free' : 'Purchase SaaS',
      hosted_link: plan.links.hosted,
      self_managed_text: free ? 'Install GitLab for free' : 'Purchase Self-managed',
      self_managed_link: plan.links.self_managed
    }
  end

  def compare_features_modal_options
    {
      id: 'compare-features-modal',
      hosted_text: 'See all features',
      hosted_link: '/pricing/gitlab-com/feature-comparison/',
      self_managed_text: 'See all features',
      self_managed_link: '/pricing/self-managed/feature-comparison/'
    }
  end
end
