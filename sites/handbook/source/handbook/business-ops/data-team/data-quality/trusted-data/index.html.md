---
layout: handbook-page-toc
title: "Trusted Data Framework"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Data Customers expect Data Teams to provide data they can trust to make their important decisions. And Data Teams need to be confident in the quality of data they deliver. But this is a hard problem to solve: the [Enterprise Data Platform](/handbook/business-ops/data-team/direction/#a-complete-enterprise-data-platform) is complex and involves multiple stages of data processing and transformation, with tens to hundreds of developers and end-users actively changing and querying data 24 hours a day. The Trusted Data Framework (TDF) supports these quality and trust needs by defining a standard  framework for data testing and monitoring across data processing stages, accessible by technical teams *and business teams*. Implemented as a stand-alone module separate from existing data processing technology, the TDF fulfills the need for an independent data monitoring solution.
   - [x] enable everyone to contribute to trusted data, not just analysts and engineers   
   - [x] enable data validations from top to bottom and across all stages of data processing
   - [x] validate data from source system data pipelines
   - [x] validate data transforms into dimensional models
   - [x] validate critical company data
   - [x] deployable independently from central data processing technology

## Key Terms

- Test Case - An [individual test](https://en.wikipedia.org/wiki/Test_case#:~:text=In%20software%20engineering%2C%20a%20test,verify%20compliance%20with%20a%20specific) and the smallest unit of a test that can be performed. In TDF the test case is expressed as a SQL statement.
- Data Schema - The tables, columns, views, and other structural elements that make up a data subject area, create using [SQL Data Definition Language](https://en.wikipedia.org/wiki/Data_definition_language#:~:text=In%20the%20context%20of%20SQL,tables%2C%20indexes%2C%20and%20users.) (DDL).
- Golden Data - [Golden data](https://blogs.informatica.com/2015/05/08/golden-record/) is a data constant from a single field or a group of fields important to the business.
- Monitoring - [Tracking the results](https://www.edq.com/glossary/data-monitoring/#:~:text=Data%20monitoring%20is%20the%20process,using%20dashboards%2C%20alerts%20and%20reports.) of tests cases to help ensure data is ready for use.

## Trusted Data Components

The primary elements of the TDF include:
1. [A Virtuous Test Cycle](/handbook/business-ops/data-team/data-quality/trusted-data/#virtuous-test-cycle) that embeds quality as a normal part of daily data development, ranging from new data solutions to break-fix issue resolution.
1. [Test Cases Expressed As SQL](/handbook/business-ops/data-team/data-quality/trusted-data/#text-cases-expressed-as-sql) which can be developed by anyone.
1. The [Trusted Data Schema](/handbook/business-ops/data-team/data-quality/trusted-data/#trusted-data-schema) saves test results for monitoring and alerting, and long-term analysis towards the path of developing wisdom around business processes and data platform performance.
1. [Schema-to-Golden Record Coverage](/handbook/business-ops/data-team/data-quality/trusted-data/#schema-to-golden-data-coverage) to provide broad coverage of the data warehouse domain, ranging from schema to critical "Golden" data.
1. The [Trusted Data Dashboard](/handbook/business-ops/data-team/data-quality/trusted-data/#trusted-data-dashboard), a *business-friendly* dashboard to visualize overall test coverage, successes, and failures.
1. The [Test Run](/handbook/business-ops/data-team/data-quality/trusted-data/#test-run) is when a Test Cases are executed.

## Virtuous Test Cycle

The TDF embraces business users as *the most important participant* in establishing trusted data and uses a very simple and accessible testing model. With SQL as a test agent, a broad group of people can contribute test cases. The test format is straightforward with simple PASS/FAIL results and just four test case types. Adoption grows quickly as TDF demonstrates value:
- Data Customers and Business Users learn the testing framework and create tests themselves
- Teams embrace testing as a valuable activity to include *at all times*, not as a last-minute activity
- The Data Team learns to add new tests as part of production-down retrospectives to more rapidly identify issues before they become large problems
- Teams develop operational rythms to continually develop new tests and expand test coverage

Over time, it is not uncommon to develop hundreds of tests cases which are run on a daily basis, continually validating data quality.

## Test Cases Expressed As SQL

SQL is the universal language in databases and nearly everyone who works with data has some level of SQL competency. The SQL tests used by TDF are very simple and designed to be accessible to anyone. Anyone who can write SQL can write a new test.

All TDF SQL tests follow the format:

```sql
SELECT
   'test_date',
   'test_name',
   'expected_result',
   'actual_result',
   'test_status' {PASS or FAIL}
FROM ....
```

## Trusted Data Schema

With all tests expressed using a standard SQL format, storing tests results is simple -- the SQL test will be prefixed with an INSERT INTO statement and output will be saved in a table. Storing test results enables a variety of valuable features, including:
- data visualization and pattern analysis test results (total tests run by date, PASS/FAIL rate by subject area, and so on)
- measurement of test coverage over a data subject or schema (number of tests by area)
- measurement of system quality improvements over time (an increase in the PASS rate)
- development of an alerting system based on test result

### Trusted Data Results

| Key | Field Name |
| ------ | ------ |
| PK | test_date |
| PK | test_name |
| | expected_result |
| | actual_result |
| | test_status |
| | database_name (optional) |
| | schema_name (optional) |
| | notes (optional) |

## Schema To Golden Record Coverage

The Data Warehouse environment changes quickly, espcially in a CI/CD environment like at GitLab. TDF supports predictability, stability, and quality with test coverage of the areas in the Data Warehouse that are most likely to change: 
   1. [Schema tests](/handbook/business-ops/data-team/data-quality/trusted-data/#schema-tests) to validate the integrity of a schema
   1. [Column Value tests](/handbook/business-ops/data-team/data-quality/trusted-data/#column-value-tests) to determine if the data value in a column matches pre-defined thresholds or literals
   1. [Rowcount tests](/handbook/business-ops/data-team/data-quality/trusted-data/#rowcount-tests) to determine if the number of rows in a table over a pre-defined period of time match pre-defined thresholds or literals
   1. [Golden Data tests](/handbook/business-ops/data-team/data-quality/trusted-data/#golden-data-tests) to determine if pre-defined high-value data exists in a table

All tests are implemented using SQL.

### Schema Tests

Schema tests are designed to validate the existence of known TABLES, COLUMNS, and other schema structures. Schema tests help identify planned and accidental schema changes.

All Schema Tests result in a PASS or FAIL status.

#### Schema Test Example

Purpose: This test validates critical tables exist in the Salesforce Data Pipeline.

```sql
SELECT 
  current_date       AS test_date,
  test_name          AS test_name,
  expected_result    AS expected_result,
  actual_result      AS actual_result,
  CASE
     WHEN actual_result = expected_result 
     THEN 'PASS' else 'FAIL' END 
                     AS test_status
FROM
    (
    SELECT 
       'schema test: Salesforce Tables' AS test_name,
       3                                AS expected_result,
       count(1)                         AS actual_result
    FROM raw.information_schema.tables
    WHERE table_schema = 'SALESFORCE_STITCH'
      AND table_name in ('ACCOUNT','OPPORTUNITY','LEAD')
    )
```

### Column Value Tests

Column Value Tests determine if the data value in a column is within a pre-defined threshold or matches a known literal. Column Value Tests are the most common type of TDF test because they have a wide range of applications. Column Value tests are useful in the following scenarios:
- change management: pre-release and post-release testing
- ensuring sums/totals for important historical data meets previously reported results
- ensuring known "approved" data always exists

All Column Value Tests result in a PASS or FAIL status.

#### Column Value Test Example 1

Purpose: This test validates we always had 6,509 Zuora account records created in 2018. 

```sql
SELECT 
  current_date       AS test_date,
  test_name          AS test_name,
  expected_result    AS expected_result,
  actual_result      AS actual_result,
  CASE
     WHEN actual_result = expected_result 
     THEN 'PASS' else 'FAIL' END 
                     AS test_status
FROM
   (
   SELECT 
       'Column Value test: Number of Zuora Accounts Created On Or Before 2019-12-31' AS test_name,
       6509                            AS expected_result,
       count(1)                        AS actual_result
    FROM raw.zuora_stitch.account
   WHERE createddate BETWEEN '2018-01-01' AND '2018-12-31'
   )
```

#### Column Value Test Example 2

Purpose: This test validates the custom field Salesforce.ACCOUNT_ID_18__C is always 18 characters long. In the past we've seed data load errors where this field has been incorrectly populated with the 15 character ID.

```sql
SELECT 
  current_date       AS test_date,
  test_name          AS test_name,
  expected_result    AS expected_result,
  actual_result      AS actual_result,
  CASE
     WHEN actual_result = expected_result 
     THEN 'PASS' else 'FAIL' END 
                     AS test_status
FROM
   (
   SELECT 
       'Column Value test: Salesforce ACCOUNT_ID_18__C is always 18 characters long' AS test_name,
       0                               AS expected_result,
       count(1)                        AS actual_result
    FROM raw.salesforce_stitch.account
   WHERE LENGTH(ACCOUNT_ID_18__C) <> 18
   )
```

### Rowcount Tests

The Rowcount test is a specialized type of Column Value test and is broken out because of its importance and utility. Rowcount tests determine if the number of rows in a table over a period of time meet expected pre-defined results. If data volumes change rapidly for legitimate reasons, rowcount tests will need to be updated appropiately.

#### Rowcount Test Example 1

Purpose: This test validates we always had 6,509 Zuora account records created in 2018. 

```sql
SELECT 
  current_date       AS test_date,
  test_name          AS test_name,
  expected_result    AS expected_result,
  actual_result      AS actual_result,
  CASE
     WHEN actual_result = expected_result 
     THEN 'PASS' else 'FAIL' END 
                     AS test_status
FROM
   (
   SELECT 
       'Column Value test: Number of Zuora Accounts Created On Or Before 2019-12-31 is 6509' AS test_name,
       6509                            AS expected_result,
       count(1)                         AS actual_result
    FROM raw.zuora_stitch.account
   WHERE createddate BETWEEN '2018-01-01' AND '2018-12-31'
   )
```

#### Rowcount Test Example 2

Purpose: We have a fast-growing business and should always have at least 100 new Subscriptions loaded from the previous day.

```sql
SELECT 
  current_date       AS test_date,
  test_name          AS test_name,
  expected_result    AS expected_result,
  actual_result      AS actual_result,
  CASE
     WHEN actual_result >= expected_result 
     THEN 'PASS' else 'FAIL' END 
                     AS test_status
FROM
   (
   SELECT 
       'Column Value test: Number of new Zuora Subscriptions is always > 100' AS test_name,
       100                              AS expected_result,
       count(1)                         AS actual_result
    FROM raw.zuora_stitch.subscription
   WHERE createddate >= current_date - 1
   )
```

### Golden Data Tests

Some data is so important it should always exist in your data warehouse and should never change: your top customer's record, the 2019 total global users count, the KPI result when you passed 1,000,000 subscriptions. Some of these cases can be solved by a developing new database capabilities, but this can be complicated and may not always match your existing data processing workflow. In addition, bugs can be accidentally added to data transforms or someone can accidentally run a bad UPDATE versus a critical production table. The Golden Data test is a specialized type of Column Value test that validates the existence of a known data literal and helps catch these problems when they occur.

#### Golden Data Test Example 1

Purpose: ACME is our most important customer. This test validates ACME is always in our DIM_ACCOUNT table.

```sql
SELECT 
  current_date       AS test_date,
  test_name          AS test_name,
  expected_result    AS expected_result,
  actual_result      AS actual_result,
  CASE
     WHEN actual_result = expected_result 
     THEN 'PASS' else 'FAIL' END 
                     AS test_status
FROM
   (
   SELECT 
       'Golden Data Test: ACME in Dim_accounts' AS test_name,
       1                                        AS expected_result,
       count(1)                                 AS actual_result
    FROM analytics.analytics_staging.dim_accounts
   WHERE account_name = 'ACME'
     AND account_status = 'Active'
     AND account_currency = 'USD'
     AND is_deleted = 'FALSE'
     AND crm_id = '0016100001BrzkTQZY'
   )
```

## Trusted Data Dashboard

The Trusted Data Dashboard is used to quickly evaluate the health of the Data Warehouse. The most important data is presented in a simple business-friendly way with clear color-coded content for test PASS or FAIL status.

![Trusted Data Dashboard](/handbook/business-ops/data-team/data-quality/trusted-data/trusted_data_dashboard.png)

## Test Run

A Test Run is when the collection of Test Cases are executed. Typically, Test Runs will be executed daily. Test Runs can also be useful for:
- testing data quality BEFORE and AFTER a major or critical schema deployment (A/B testing)
- establishing a quality baseline before executing a large [SQL Data Manipulation Language](https://en.wikipedia.org/wiki/Data_manipulation_language) (DML) operation

| Test Run Type | Purpose |
| ------ | ------ |
| Daily Validation | Run a nightly Test Run to ensure all data passes validation. |
| Release Validation | Execute a Test Run prior to a major DML or DDL change AND after the change and compare results. |
| Spot-Check Validation | Execute a subset of Test Cases to validate a specific subject area. |
