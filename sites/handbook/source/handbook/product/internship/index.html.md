---
layout: handbook-page-toc
title: Product Internship - Best Practices
---

This document describes best practices for [internship for learning](/handbook/people-group/promotions-transfers/#internship-for-learning/) with Product.

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Interning for Learning - Product Best Practices

1. Get manager approval from the intern’s existing manager, identify an IC PM to be the ‘Intern Mentor’, identify the IC’s manager as the ‘Intern Manager’ and define a start date.
1. Set an expectation with the intern’s current manager that the intern will give > 40% of their time to the internship.
1. Create on-going 1:1 with the Intern Mentor to foster regular communication regarding intern role and responsibilities.
    1. See [internship agenda template](https://docs.google.com/document/d/1NnCo8iNtLkBAZH6FnTHjjc1S0UVCcncYGHk3wFnEtTg/edit#heading=h.5mqqjbquuysm) for reference
1. Before the start date, provide the intern with resources such as books and articles relating to Product Management.
1. Add intern to the email groups, gitlab groups, relevant slack channels and recurring meetings.
1. Discuss workload with current manager in 1:1s to ensure workload percentage between both roles is balanced.
1. Add structure by setting deadlines and reviewing action items regularly in 1:1s with the Intern Mentor and/or Intern Manager.
    1. Define actionable results for the intern to focus on.
1. The Intern Manager and Mentor will prioritize the intern like an additional team member and provide regular feedback and guidance.
