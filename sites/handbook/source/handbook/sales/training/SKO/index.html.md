---
layout: handbook-page-toc
title: "Sales Kick Off"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SKO Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

# Sales Kick Off 2021
SKO 2021 is scheduled to take place during the week of Feb 8-12, 2021 in Denver, CO USA. More details coming soon!

## High-level SKO 2021 Agenda 
*  Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO
*  Mon, 2020-02-08: Travel day plus evening Welcome Reception
*  Tue, 2020-02-09: Keynote Q&As, speakers, and evening Awards Dinner
*  Wed, 2020-02-10: Interactive role-based training breakouts
*  Thu, 2020-02-11: Regional QBRs
*  Fri, 2020-02-12: Travel day to return home
All subject to change. 

## SKO 2021 FAQ 
**Q: What are the dates and location?**

A: Sales Kick Off (SKO) will take place from 2021-02-08 - 2021-02-12 in Denver, CO. The event will be held at the [Sheraton Downtown Denver](https://www.marriott.com/hotels/travel/dends-sheraton-denver-downtown-hotel/). 

**Q: How are we planning for this event in light of COVID-19?** 

A: We are planning an in-person event with a hybrid solution – in-person and livestream for team members who are not able to or are not comfortable traveling for the event. We are actively monitoring the COVID-19 crisis and consulting with our security vendor leading up to the event with the health and safety of our team members as our top priority. The SKO planning team is working toward a deadline of mid-November for a go/no-go call. If it is deemed unsafe to gather in-person for this event, we will default to a fully-virtual SKO. You will not be asked to book travel until this decision has been made. 

**Q: When and how should I book travel?**

A: Please do not book travel at this time. Once registration is launched, you will be contacted by our travel vendor via email to book your travel. 

**Q: Do I need to get a visa letter now or should I wait until November?**

A: We are actively working with People Ops to determine the appropriate process and timing for visas and will update this FAQ as well as the #sko-denver-2021 Slack channel when we know more. 

**Q: What is the agenda?**

A: Please see the section above for a high-level agenda. We will post the final agenda in the event app at a later date. 

**Q: Will there be any pre-work?**

A: In support of GitLab’s all-remote culture, there will be pre-work that all SKO participants will be asked to complete to make the best use of our time together in person. Details will be shared in early Q4. 

**Q: What will I be expected to participate in?**

A: SKO is a rare opportunity for our team to get together and prepare for the year ahead. As such, the agenda is purposely crafted with numerous activities to learn, network, and celebrate together, and we kindly ask and expect that every SKO attendee participates in all that SKO has to offer. 

Based on feedback from SKO 2020, the planning team is building out more break time to allow team members to catch up on work and recharge. 

**Q: How will we include partners in SKO 2021?**

A: We will host a Partner Summit in tandem with SKO. Partners will be invited to participate in our Day 1 keynotes on Tuesday, and they will have their own Partner Summit-specific sessions on Wednesday. Partners will be available to meet and greet with GitLab team members in the Partner Pavillion on Tuesday and Wednesday nights prior to dinner. 

**Q: Are guests welcome?**

A: Unlike Contribute, guests are not permitted to participate in SKO activities, as we want to keep the focus on learning, networking, and planning for the year ahead. For this reason, we do not recommend including guests between 2021-02-09 - 2021-02-11. Guests are welcome to join before or after SKO.

**Q: Where can I ask questions related to SKO?**

A: Please Slack #sko-denver-2021 or you can email sales-events at gitlab dot com with questions. Members of the core SKO planning team, including Libby Schulze, David Somers and Monica Jacob are actively monitoring this email and will respond to your inquiry as soon as possible. 

We are actively planning this event and will update this FAQ as more information becomes available. 

Reference: [Company Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)



# Sales Kick Off 2020
In February 2020, the entire sales organization and their supporting teams gathered in Vancouver, BC Canada for the inaugural GitLab Sales Kick Off. This page includes slide decks, videos, and pictures from the event.

## SKO 2020 Theme
**Level Up** - It works across nationalities / languages and has lots of ways we can tie it into the content. All about leveling up the business.

## SKO 2020 Day 1 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Welcome & FY21 Overall GitLab Strategy & Vision | [slides](https://drive.google.com/open?id=1mvqXfa3vIQec8mhBSzVB5gZDC7LJwdMhjHev0nkVj9g) (GitLab internal) | [video](https://youtu.be/kc3H0gMC7u4) (GitLab internal) |
| Product Strategy & Vision with Q&A | [slides](https://drive.google.com/open?id=1DfsjznCwqBz9MpueftriC1QS81QHuXcdAACqtdcFPU0) (public) | [video](https://youtu.be/fS5T-XOvXPk) (public) |
| FY21 Sales & Marketing Strategy & Vision with Q&A | [slides](https://drive.google.com/open?id=1bq_bxM07PTNF3rfAbFB_Fia7U8CszfLTEJUrF-bfilI) (GitLab internal) | [video](https://youtu.be/8vrp1AGRp1U) (GitLab internal) |
| CRO Staff Level Up Panel Discussion | no slides | [video](https://youtu.be/EYlC9uP3LoE) (GitLab internal) |
| Leveling Up with Partners at GitLab | [slides](https://drive.google.com/open?id=12j219pElrox1hUyMpiOwSSrTV6S6biLurahKuLjvH1Y) (GitLab internal) | [video](https://youtu.be/jMUzgPIfFXg) (GitLab internal) |
| Keys to Winning panel discussion | [slides](https://drive.google.com/open?id=1V1lDVIJyX1mMin2Hm7AaExps0WAgczi6vAB6UWFuR8c) (GitLab internal) | [video](https://youtu.be/ervvabavL2o) (GitLab internal) |
| Sales Kick Off Awards Ceremony | [slides](https://drive.google.com/open?id=1deR4D2GplTGan1E2ENZbhZ0rt4YiUzZEJGX7RLj83yY) (GitLab internal) | [video](https://drive.google.com/open?id=1_lbLGvYhhB6ynyaWngeuE6nkDUcUXv1A) (GitLab internal) |


## SKO 2020 Day 2 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Day 2 Welcome | no slides | [video](https://youtu.be/bI1XBeZajIs) (GitLab internal) |
| Articulating and Selling the GitLab Vision & Customer Journey | [slides](https://drive.google.com/open?id=14kO1iTqSwuvV-7CDHThslUv8_QP_EnaZg4yG-1romZc) (GitLab internal) | [video](https://youtu.be/DtL38mgpycE) (GitLab internal) |
| Getting Into Accounts That Say They Don’t Have a Problem | [slides](https://drive.google.com/open?id=1dRdKs7BkbyTzTfFZ4JwwjJUIwgTSSOjkG-DEGDGgN30) (public) | N/A (not recorded) |
| Proactively Competing Against Microsoft | [slides](https://drive.google.com/open?id=1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8) (GitLab internal) | [video](https://youtu.be/Ds_U8iiUOz8) (GitLab internal) |
| Customer Expectations and Storytelling | [slides](https://drive.google.com/open?id=1hnYT7ulTPpb7C3V_fKdOU9gajAA1oufzp9EiDYQC-mU) (public) | N/A (not recorded) |
| Finding and Defining the Customer’s Problem | [slides](https://drive.google.com/open?id=18NynGuJTEwSUnKkRR4A1fQa2xojPxpO3mlnvb3BmMXY) (public) | [video](https://youtu.be/_RSqUgYDjdQ) (GitLab internal) |
| One Lab, One Story: Leveling Up Our Demo Labs | [slides](https://drive.google.com/open?id=1x125pYEjAQQX5vDo5u2eanAoVtoz5R2muiaYqXL_xEE) (public) | N/A (not recorded) |
| Customer Success Plan Workshop | [slides](https://drive.google.com/open?id=15Qt-UcfRt9cX-4CV7zMsurojTZg_8Kf-u0dMYL16JXQ) (public) | N/A (not recorded) |
| Gaining Access and Pitching to the Economic Buyer | [slides](https://drive.google.com/open?id=166GA0LyvQLG6y-9qAuKoJ6iZUxmjpjklLSZ1J9wC6z0) (public) | N/A (not recorded) |
| Command Plan Excellence | [slides](https://drive.google.com/open?id=1Vjn5ICOpwxbZNFRZhvpwZc0REkHMeEKNSakpFV04EuM) (public) | [video](https://youtu.be/zN_0J6syxmM) (GitLab internal) |
| Technically Competing Against MSFT in CI Use Case | [slides](https://drive.google.com/open?id=1Rfsk5_h5O6DF14wjgmd7WrzQPCC1yGzYPPdYG_JVApg) (GitLab internal) | N/A (not recorded) |
| Next Level Engagements: Consulting Acumen | [slides](https://drive.google.com/open?id=1ahDNo93BpNSRonLj6C3iWKZfeJy1ZfJZosujpzSzo2U) (public) | N/A (not recorded) |
| Proactively Competing Against Jenkins | [slides](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg) (GitLab internal) | N/A (not recorded) |
| Account Planning | [slides](https://drive.google.com/open?id=1XPhv3zqtR-q5sKgDx1HhnNkaQUSIVTO0pXj2wPdjafs) (public) | [video](https://youtu.be/-vPkzlWLZ8o) (GitLab internal) |
| From SCM and CI to Security: Paths to Ultimate | [slides](https://docs.google.com/presentation/d/1VTCfKcAZ4NS2xQu8C-fA6pugCdvUbb0MfQhimLiAGak/) (public) | N/A (not recorded) |
| Next Level Engagements: Consulting Methodology | [slides](https://drive.google.com/open?id=1TWg_grUCegOnJK8yCXxq7lRgVCByoJMCd59u3A1QjMo) (public) | N/A (not recorded) |


## SKO 2020 Wrap Up
* [SKO Wrap Up presentation](https://docs.google.com/presentation/d/1MwJRWCGl-U2qic_h3xHQxGDCUf1s0R23aKEIrXqcXW0/edit?usp=sharing) (public)
* [GitLab FY21 Sales Kick Off Wrap Up and Survey Results](https://youtu.be/_q9M9_nwNy4) video (public) 

## GitLab Infomercial
* [Public version on YouTube](https://youtu.be/gzYTZhJlHoI) featuring David Astor
* [Long version](https://drive.google.com/open?id=1yhG_JLh4rpayRvJYkYE4EJYMcNZkrUXd) (GitLab internal)

## SKO 2020 Pictures
* [Sales Kick Off photo album](https://photos.app.goo.gl/hcvEyzH3wDdccrQw7)
* [LinkedIn headshots](https://drive.google.com/open?id=1_Laql3qZBj9hp6CXrHCs7ovrCQOGR1gx) 
