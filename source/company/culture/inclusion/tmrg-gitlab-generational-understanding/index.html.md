---
layout: handbook-page-toc
title: "TMRG - Generational Understanding"
description: "We are the Generational Understanding Resource Group (TMRG) founded in the summer of 2020. Learn more!"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-generational-understanding/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission

The mission of the generational understanding [TMRG](/company/culture/inclusion/erg-guide/) is to ensure that team members of all ages feel they have a safe space to communicate, unite and share issues that are reflective of their experience while at GitLab, and to encourage a work culture where GitLab team members of all ages are valued, empowered, and given opportunities to develop and contribute to their full potential. This culture will be a competitive advantage for GitLab, as it helps GitLab become a thought leader on this topic. It will leverage the wisdom of all team members, as it helps identify ongoing patterns as well as best practices for mentoring.

## Leads

* [Francis Potter](https://about.gitlab.com/company/team/#francispotter)
* [Wayne Haber](https://about.gitlab.com/company/team/#whaber)

## Communications

* Slack Channel: [#generational_understanding](https://gitlab.slack.com/archives/C014A4NNVG8/p1596576533036100)
* Tri-Weekly Group Meeting
  * [Agenda](https://docs.google.com/document/d/1h81q60KnCJFsATKjZKOFglBmgkv8TlGrKY8punX2SEg/edit#heading=h.hypr6mscnzqs)
* [Google Group](https://groups.google.com/a/gitlab.com/g/Generational_Differences_ERG)

## Links

Links to interesting handbook pages, articles, books, and training

### Handbook

* [Speaking with TMRG members in the hiring process](/company/culture/inclusion/#speaking-with-tmrg-members-in-the-hiring-process)
* [Team member identity data](/company/culture/inclusion/identity-data/#age-distribution)

### Articles

* [Arxiv: Is 40 the new 60? How popular media portrays the employability of older software developers](https://arxiv.org/ftp/arxiv/papers/2004/2004.05847.pdf)
* [Wired: Surviving as an Old in the Tech World](https://www.wired.com/story/surviving-as-an-old-in-the-tech-world/)
* [LinkedIn: Google Leaders & "Greyglers"​ On Navigating Our Way to Bigger and Better Things](https://www.linkedin.com/pulse/google-leaders-greyglers-navigating-our-way-bigger-better-tracy-wilk/)
* [U.S. Equal Employment Opportunity Commission: Age Discrimination](https://www.eeoc.gov/age-discrimination)
* [Psychology Today:"Young People Are Just Smarter"](https://www.psychologytoday.com/us/blog/boomers-30/201710/young-people-are-just-smarter)

### Training courses

* [LinkedIn:Thriving across our differences](https://www.linkedin.com/learning/confronting-bias-thriving-across-our-differences/outro-with-arianna-huffington)

### Books

* [Wisdom at Work: The Making of a Modern Elder](https://www.amazon.com/Wisdom-Work-Making-Modern-Elder/dp/0525572902)

