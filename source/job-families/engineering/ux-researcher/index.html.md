---
layout: job_family_page
title: "UX Researcher"
---

At GitLab, UX Researchers collaborate with our Product Designers, Product Managers and the rest of the community to assist in determining what features should be built, how they should behave, and what the priorities should be. UX Researchers report to the UX Research Manager.

## Responsibilities

Unless otherwise specified, all UX Research roles at GitLab share the following requirements and responsibilities:

#### Requirements

* [Self-motivated and self-managing](https://about.gitlab.com/handbook/values/#efficiency), with strong organizational skills.

* Share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

* Passionate about mentoring others.

* Evangelize research. Share user insights within the broader organization and externally in creative ways to increase empathy.

* Empathetic, curious and open-minded.

* Able to thrive in a fully remote organization.

* Able to use GitLab.

#### Nice to haves

* Enterprise software company experience.

* Developer platform/tool industry experience.

* A degree in psychology, human factors, human-computer interaction, or a related field.

### UX Research Coordinator

#### Job Grade

The UX Research Coordinator is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Manage all aspects of participant recruitment for GitLab’s user experience research studies including, but not limited to, sourcing, outreach, screening, scheduling, participation agreements, and incentives management.

* Grow and administer GitLab First Look, our participant recruitment database.

* Refine, document and maintain processes related to participant recruitment.

* Collaborate with Product Designers, Product Managers, and UX Researchers to effectively understand their research goals and recruit appropriate participants.

* Be the Social Media Ambassador for UX Research at GitLab. Maintain social media accounts that promote GitLab's research efforts and aid participant recruitment.

* Manage relationships with third-party providers, such as recruitment platforms, digital rewards solutions, etc.

* Seek out feedback from Senior and Staff UX Researchers to grow your skillset.

* Respond to inquiries from research participants.

### UX Researcher

#### Job Grade

The UX Researcher is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Deeply understand the technology and features of the stage groups to which you are assigned.

* Maintain a thorough knowledge of the direction and vision for your assigned stage groups. Consistently consider how your research can support the goals of your assigned stage groups, the broader product vision, and company objectives.

* Conduct problem validation user research projects within your stage groups, and engage Product Managers for collaboration.

* Respond to solution validation user research inquiries that are escalated by Product Design Managers.

* Proactively identify strategic user research initiatives that span multiple stage groups, and engage Senior UX Researchers/Staff UX Researchers for guidance and/or collaboration.

* Actively contribute to UX Research processes and documentation.

### Senior UX Researcher

#### Job Grade

The Senior UX Researcher is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Demonstrate and articulate understanding of the technology and features of immediate and adjacent stage groups.

* Maintain a thorough knowledge of the direction and vision for immediate and adjacent stage groups. Consistently consider how your research can support the goals of immediate and adjacent stage groups, the broader product vision, and company objectives.

* Conduct problem validation user research projects within your stage groups, and engage Product Managers for collaboration.

* Respond to solution validation user research inquiries are escalated by Product Design Managers.

* Lead strategic user research initiatives that span multiple stage groups.

* Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade-offs between smart, scrappy research and rigor.

* Actively contribute to UX Research processes and documentation.

* Work in collaboration with Staff UX Researchers to design and deliver training content and workshops.

* Interview potential UX candidates.

### Staff UX Researcher

#### Job Grade

The Staff UX Researcher is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Demonstrate and articulate understanding of the technology and features of immediate and adjacent stage groups, and have a working knowledge of the end-to-end GitLab product.

* Maintain a thorough knowledge of the direction and vision for immediate and adjacent stage groups. Consistently consider the holistic user experience and how your research can support the goals of immediate and adjacent stage groups, the broader product vision, and company objectives.

* Conduct problem validation user research projects within your stage groups, and engage Product Managers for collaboration.

* Respond to solution validation user research inquiries that are escalated by Product Design Managers.

* Lead strategic user research initiatives that span multiple stage groups (and possibly the entire product).

* Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade-offs between smart, scrappy research and rigor.

* Actively seek out difficult impediments to our efficiency as a team (process, tooling, etc), and propose and implement solutions that will enable the entire team to work more efficiently.

* Exert significant influence on the overall objectives and long-range goals of the UX Research team.

* Identify research training needs for the UX and Product Department. Work with Senior UX Researchers to design and deliver training content and workshops.

* Interview potential UX candidates.

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Performance indicators

* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)

* [Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)

## Relevant links

- [UX Research Handbook](/handbook/engineering/ux/ux-research/)

- [UX Department Handbook](/handbook/engineering/ux/)

- [Product Handbook](/handbook/product)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters. In this call we will discuss your experience, understand what you are looking for in a UX Research role, discuss your compensation expectations, reasons why you want to join GitLab and answer any questions you have.

* Next, if a candidate successfully passes the screening call, candidates will be invited to schedule a 45 minute first interview with a UX Researcher. Overall, this stage aims to understand your research methods, how you choose a methodology to use, how you present results, and how you work with a wider team of counterparts. This interview will also look to understand the softer skills you have as a researcher, and how you apply these in the real world. Please be prepared to present a case study of your work at this stage, too.

* If you successfully pass the previous stage, candidates will then be invited to schedule a 1-hour interview with the UX Research Manager. In this interview, we will be looking for you to give some real insight into two projects you've worked on. We'll look to understand the size and structure of the team you were a part of, the goals of the project, how you approached research, how you synthesized research data in order to inform design/product decisions and the final output of your research.

* Candidates will be invited to schedule a third 50-minute interview with our UX Director if they successfully pass the previous interview. In this final stage interview, we'd like to see the end result of a research study you've been a part of, we want to understand the question(s) you were addressing (and how you determined these questions), your methodology, and how you shared that information with the wider team.

* Successful candidates will subsequently be made an offer via a video call or phone call.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

[groups]: /company/team/structure/#groups
